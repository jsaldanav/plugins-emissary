<?php
/**
 * Plugin Name: Emissary for Woocommerce
 * Plugin URI: https://emissary.mx/
 * Description: El software esencial de logística para Ecommerce, muestra el costo de envío a tus clientes de manera dinámica en base a su código postal y peso total de su envío, ¡También incluye las zonas extendidas de cada paquetería!
 * Version: 1.0
 * Author: EMISSARY 
 * Author URI: https://emissary.mx/
 * Text Domain: emissary.php
 * Domain Path: /languages
 * License: 
 */

add_filter( 'wc_get_template', function ( $file, $name ) {
    if ( $name === 'order/order-details.php' ) {
        $file = __DIR__ . '/public/templates/order-details.php';
    }
    return $file;
}, 10, 2 );

add_filter( 'wc_get_template', function ( $file, $name ) {
    if ( $name === 'cart/cart-shipping.php' ) {
        $file = __DIR__ . '/public/templates/cart-shipping.php';
    }
    return $file;
}, 10, 2 );


add_filter( 'plugin_row_meta', 'emissary_pluginRowMeta', 10, 2 );
function emissary_pluginRowMeta( $links, $file ) {
    global $EMISSARY_lang;
    if ( plugin_basename( __FILE__ ) == $file ) {
        $row_meta = array(
            'documentation'    => '<a href="'.esc_url( 'https://emissary.mx/' ) .' "target="_blank" >' . esc_html__( __($EMISSARY_lang->documentation, EMISSARY_PLUGIN), 'domain' ) . '</a>',
            'premium_support'    => '<a href="'.esc_url( 'https://emissary.mx/' ) .' "target="_blank" >' . esc_html__( __($EMISSARY_lang->premium_support, EMISSARY_PLUGIN), 'domain' ) . '</a>'
        );
        return array_merge( $links, $row_meta );
    }
    return (array) $links;
}

if( !function_exists('is_plugin_active') ) {
    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}

if( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
    if (!defined('ABSPATH')) {
        exit;
    }

    if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
        exit;
    }

    if (!defined('EMISSARY_FILE')) {
        define('EMISSARY_FILE', __FILE__);
    }

    if (!defined('EMISSARY_PLUGIN')) {
        define('EMISSARY_PLUGIN', 'emissary-woocommerce.php');
    }

    global $EMISSARY_lang;
    if(!isset($EMISSARY_lang)) {
        $EMISSARY_lang = emissary_getLanguages();
    }

    require_once __DIR__."/includes/emissary.php";
    require_once __DIR__."/admin/emissary-admin.php";

    function emissary_adminStyle () {
        wp_register_style('emissary-css', plugins_url('admin/css/style.css', EMISSARY_FILE));
        wp_enqueue_style('emissary-css', plugins_url('admin/css/style.css', EMISSARY_FILE));
    }
    add_action('admin_head', 'emissary_adminStyle');

    function emissary_frontCSS () {
        wp_register_style('emissary-css-public', plugins_url('public/css/emissary_style.css', EMISSARY_FILE));
        wp_enqueue_style('emissary-css-public', plugins_url('public/css/emissary_style.css', EMISSARY_FILE));
    }
    add_action( 'wp_enqueue_scripts', 'emissary_frontCSS' );

    add_filter('plugin_action_links_' . plugin_basename(EMISSARY_FILE), ['Emissary', 'plugin_settings_link']);

    add_filter('woocommerce_shipping_methods',  ['Emissary', 'shipping_methods']);

    add_filter('allowed_http_origins', 'emissary_addAllowedOrigins');
    function emissary_addAllowedOrigins($origins) {
        $origins[] = get_site_url();
        return $origins;
    }

    register_activation_hook( __FILE__, array( 'Emissary', 'activate' ) );

    function emissary_endPoints() {
        add_rewrite_endpoint( 'webhook', EP_ROOT | EP_PAGES );
    }
    add_action( 'init', 'emissary_endPoints' );

    add_filter('woocommerce_billing_fields', 'emissary_removeBillingPhoneField', 20);
    function emissary_removeBillingPhoneField($fields) {
        $fields ['billing_phone']['required'] = true;

        return $fields;
    }

    add_filter('woocommerce_package_rates','emissary_runtimeAddShippingMethords',100,2);
    function emissary_runtimeAddShippingMethords($rates,$package) {

        return $rates;
    }

} else {
    deactivate_plugins('emissary/emissary.php');
}

function emissary_getLanguages ($returnArray = false){
    $dir = __DIR__.'/languages/';
    if (is_dir($dir)){
        $request = file_get_contents( __DIR__.'/languages/en_US.json');
        if ($languagesDirectory = opendir($dir)){
            while (($file = readdir($languagesDirectory)) !== false) {
                if ($file === get_locale().'.json') {
                    $request = file_get_contents( __DIR__.'/languages/'.get_locale().'.json');
                }
            }
            closedir($languagesDirectory);
        } else {
            error_log("LOCALISATION_ERROR: can't open the dir \"{$dir}\"");
        }
    } else {
        error_log("LOCALISATION_ERROR: can't find the dir \"{$dir}\"");
        return false;
    }

    if(!$request){
        error_log("LOCALISATION_ERROR: error in lang file");
        return false;
    }

    return json_decode($request, $returnArray);
}
?>