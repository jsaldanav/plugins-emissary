<?php

include_once(ABSPATH . 'wp-admin/includes/plugin.php');

function emissary_shipping_init() {
    global $EMISSARY_lang;
    $excludedZone = false;

    if (!class_exists( 'Emissary_Shipping_Method')) {
        class Emissary_Shipping_Method extends WC_Shipping_Method {
            protected $emissary_accessFormFields;

            public $parsels;
            public static $instance;

            public function __construct() {
                global $EMISSARY_lang;
                parent::__construct();
                $this->EMISSARYTools = new Emissary;
                $this->id                 = 'emissary';
                $this->title              = $EMISSARY_lang->menu_name;
                $this->enabled            = 'yes';
                $this->api_key = isset($settings['key_production']) ? $settings['key_production'] : '';

                $this->init();
                self::$instance = $this;
                return $this;
            }

            public static function get() {
                if (self::$instance === null) {
                    self::$instance = new self();
                }
                return self::$instance;
            }

            function init() {
                $this->init_settings();
                $this->emissary_accessFormFields = $this->emissary_Form($this->settings);

                $this->form_fields = array_merge(
                    $this->emissary_accessFormFields
                );

                add_action( 'woocommerce_update_options_shipping_' . $this->id, [$this, 'process_admin_options']);

                add_action( 'admin_emissary_scripts', [$this, 'js']);
                add_action( 'admin_emissary_scripts', [$this, 'css']);
            }

            public function calculate_shipping( $package = array() ) {
                global $EMISSARY_lang;
                $setting = get_option('woocommerce_emissary_settings');
                $cp_origin = $setting['cp_origin'];
                $weight = 0;
                $cost = 0;
                if (isset($package['rates'])) {
                    foreach ($package['rates'] as $key => $value) {
                        unset($package['rates'][$key]);
                    }
                }
                $dest_country = $package["destination"]["country"];
                $dest_postcode = $package["destination"]['postcode'];
                $dest_statecode = $package["destination"]['state'];
                $dest_city = $package["destination"]['city'];

                $parsels = $this->calculate_weight($package);

                $origin_address     = get_option( 'woocommerce_store_address' ) . ', ' . get_option( 'woocommerce_store_address_2' );
                $origin_city        = get_option( 'woocommerce_store_city' );
                $origin_postcode    = get_option( 'woocommerce_store_postcode' );
                $store_raw_country = get_option( 'woocommerce_default_country' );
                $split_country = explode( ":", $store_raw_country );
                $origin_country = $split_country[0];
                $origin_state = $split_country[1];

                $shippingServices = [];
                
                $request = json_encode(array(
                    "shippers"=> $shippingServices,
                    "parcels"=> $parsels,
                    "address_from"=> array(
                        "city"=> $origin_city,
                        "company"=> "",
                        "country"=> $origin_country,
                        "email"=> "",
                        "name"=> "",
                        "phone"=> "",
                        "state"=> $origin_state,
                        "street1"=> $origin_address,
                        "postal_code"=> $origin_postcode
                    ),
                    "address_to"=> array(
                        "city"=> $dest_city,
                        "company"=> "",
                        "country"=> $dest_country,
                        "email"=> "",
                        "name"=> "",
                        "phone"=> "",
                        "state"=> $dest_statecode,
                        "street1"=> "",
                        "street2"=> "",
                        "postal_code"=> $dest_postcode
                    ),
                    "origin"=>get_bloginfo('url'),
                    "cp_origin"=>$cp_origin
                ));
                $has_free_shipping = false;
                $applied_coupons = WC()->cart->get_applied_coupons();
                foreach( $applied_coupons as $coupon_code ){
                    $coupon = new WC_Coupon($coupon_code);
                    if($coupon->get_free_shipping()){
                        $has_free_shipping = true;
                        break;
                    }
                }
                $itemsincart=0;
                $virtualitems=0;
                foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {  
                    $product = $cart_item['data'];
                    $itemsincart++;
                    if($product->is_virtual()){
                        $virtualitems++;
                    }
                }
                if($virtualitems==$itemsincart){
                    $has_free_shipping=true;
                }
                $EmissaryObj = new Emissary();
                if($has_free_shipping){
                    //$response = $EmissaryObj->emissary_request('/Aplicacion/clases/Integraciones/Woocommerce/rates.aspx', 'POST', $request);
                    $rate_list = array();
                    $settings = get_option('woocommerce_emissary_settings');
                    //$extra_mount = $settings['extra_mount'];
                    //if (!empty($response) && empty($response->errors)) {
                        $setting = get_option('woocommerce_emissary_settings');
                        $tax = get_option('woocommerce_calc_taxes');
                        //foreach ($response as $key => $carrier_rate) {
                            //if (!empty($carrier_rate)) {
                                //if (isset($carrier_rate->quoteId)) {
                                    $currency = get_woocommerce_currency();

                                    $_price = 0;
                                    $logo = "<img src='https://emissary.mx/images/shippers/envio-gratis.png' title='Envío gratuito' class=\"shipment_logo_options\">";
                                    $description1 = $logo . "<span class=emissary_service>Envío gratuito</span> <span class=emissary_amount>$0</span>";

                                    $rate = array(
                                        'id' => '1',
                                        'label' => 'Envío gratuito',
                                        'cost' => 0,
                                        'user' => null,
                                        'meta_data' => array(
                                            'label_description_Advance' => base64_encode($description1),
                                            'quoteId' => 0,
                                            'rateQuoteId' => 0,
                                            'shippingMethod' => 'Envío gratuito',
                                            'shippingMethodStore' => 'Envío gratuito',
                                            'deliveryCost' => 0,
                                            "totalAmountPesos" => 0,
                                            "taxes" => 0,
                                            "shipper" => 'Envío gratuito',
                                            "otherCharges" => 0,
                                            "totalAmount" => 0,
                                            "currency" => 'MXN'
                                        )
                                    );

                                    $rate_list[] = $rate;
                                //}
                            //}
                        //}
                    //}
                }else{
                    $response = $EmissaryObj->emissary_request('/Aplicacion/clases/Integraciones/Woocommerce/rates.aspx', 'POST', $request);
                    $rate_list = array();
                    $settings = get_option('woocommerce_emissary_settings');
                    $extra_mount = $settings['extra_mount'];
                    $deliver_in_store = $settings['deliver_in_store'];
                    //var_dump($settings);
                    if (!empty($response) && empty($response->errors)) {
                        $setting = get_option('woocommerce_emissary_settings');
                        $tax = get_option('woocommerce_calc_taxes');
                        foreach ($response as $key => $carrier_rate) {

                            if (!empty($carrier_rate)) {
                                if (isset($carrier_rate->quoteId)) {
                                    $currency = get_woocommerce_currency();

                                    $_price = $carrier_rate->totalAmountPesos+(int)$extra_mount;
                                    $logo = "<img src='". $carrier_rate->carrierLogoUrl ."' title='". $carrier_rate->shippingMethodStore ."' class=\"shipment_logo_options\">";
                                    $description1 = $logo . "<span class=emissary_service>$carrier_rate->shippingMethodStore</span> <span class=emissary_amount>$ $_price</span>";

                                    $rate = array(
                                        'id' => str_replace(' ', '_', $carrier_rate->rateQuoteId),
                                        'label' => $carrier_rate->shippingMethod,
                                        'cost' => $_price+(int)$extra_mount,
                                        'user' => isset($package['user']) && isset($package['user']->id) ? $package['user']->id : null,
                                        'meta_data' => array(
                                            'label_description_Advance' => base64_encode($description1),
                                            'quoteId' => $carrier_rate->quoteId,
                                            'rateQuoteId' => $carrier_rate->rateQuoteId,
                                            'shippingMethod' => $carrier_rate->shippingMethod,
                                            'shippingMethodStore' => $carrier_rate->shippingMethodStore,
                                            'deliveryCost' => $_price,
                                            "totalAmountPesos" => $carrier_rate->totalAmountPesos+(int)$extra_mount,
                                            "taxes" => $carrier_rate->taxes,
                                            "shipper" => $carrier_rate->shipper,
                                            "otherCharges" => $carrier_rate->otherCharges,
                                            "totalAmount" => $carrier_rate->totalAmount+(int)$extra_mount,
                                            "currency" => $carrier_rate->currency
                                        )
                                    );

                                    $rate_list[] = $rate;
                                }
                            }
                        }
                        if($deliver_in_store=="yes"){
                            $logo = "<img src='https://emissary.mx/images/shippers/envio-gratis.png' title='Envío gratuito' class=\"shipment_logo_options\">";
                            $description1 = $logo . "<span class=emissary_service>Recoger en tienda</span> <span class=emissary_amount>$0</span>";
                            //echo $deliver_in_store;
                            $rate = array(
                                'id' => '100',
                                'label' => 'Recoger en tienda',
                                'cost' => 0,
                                'user' => null,
                                'meta_data' => array(
                                    'label_description_Advance' => base64_encode($description1),
                                    'quoteId' => 0,
                                    'rateQuoteId' => 0,
                                    'shippingMethod' => 'Recoger en tienda',
                                    'shippingMethodStore' => 'Recoger en tienda',
                                    'deliveryCost' => 0,
                                    "totalAmountPesos" => 0,
                                    "taxes" => 0,
                                    "shipper" => 'Recoger en tienda',
                                    "otherCharges" => 0,
                                    "totalAmount" => 0,
                                    "currency" => 'MXN'
                                )
                            );

                            $rate_list[] = $rate;
                        }
                    }
                }
                usort($rate_list, function($a, $b) { return $a['cost'] > $b['cost']; });
                foreach ($rate_list as $rate) {
                    $this->add_rate($rate);
                }

            }

            public function calculate_weight($package = array()) {

                $_parcels = array();
                $_length = 0;
                $_height = 0;
                $_width = 0;
                $_weight = 0;
                $_qty = 0;

                foreach ( $package['contents'] as $item_id => $values ) {
                    if(!empty($values['data'])) {
                        $_Q = $values['quantity'];
                        $product = $values['data'];
                        $tmpLength = 0;
                        $tmpHeight = 0;
                        $tmpWidth = 0;

                        $thisParcel = null;
                        
                        $_weight = (float)$product->get_weight();
                        $_qty = (float)$_Q;
                        $tmpLength = (float)$product->get_length();
                        $tmpHeight = (float)$product->get_height();
                        $tmpWidth = (float)$product->get_width();


                        $weightUnit = get_option('woocommerce_weight_unit');
                        $dimensionUnit = get_option('woocommerce_dimension_unit');

                        $originalWeight = $addPackagingWeight === 'yes' ? ( $_weight + $standard_packaging_weight ) : $_weight;

                        if ($dimensionUnit === 'cm' && $tmpHeight < 1) {
                            $tmpHeight = 1;
                        }

                        if ($weightUnit === 'g') {
                            $originalWeight /= 1000;
                            $weightUnit = 'kg';
                        }

                        if ($weightUnit === 'oz') {
                            $originalWeight /= 35.274;
                            $weightUnit = 'kg';
                        }

                        $endWeight = round($originalWeight, 1);

                        if ($endWeight <= 0) {
                            $endWeight = 0.1;
                            $weightUnit = 'kg';
                        }

                        $thisParcel = array(
                            'quantity' => $_qty,
                            'weight' => $endWeight,
                            'weight_unit' => $weightUnit,
                            'length' => $tmpLength,
                            'height' => $tmpHeight,
                            'width' => $tmpWidth,
                            'dimension_unit' => $dimensionUnit,
                        );
                        $_parcels[] = $thisParcel;
                        
                    }
                }
                return $_parcels;
            }

            public function js() {
                wp_register_script('emissary-admin-ajax', plugins_url('admin/js/ajax.js', EMISSARY_FILE), array('jquery'), '1.13');
                wp_enqueue_script( 'emissary-admin-ajax');
            }

            public function css() {
                wp_enqueue_style( 'emissary-admin-css', plugins_url('admin/css/style.css', EMISSARY_FILE) );
            }

            public function admin_options() {
                global $woocommerce;
                $php_version    = PHP_VERSION;
                $wp_version     = get_bloginfo('version');
                $wc_version     = $woocommerce->version;
                $server_name    = $_SERVER['SERVER_NAME'];
                $curl_exists    = class_exists('WP_Http_Curl') ? 'Yes': 'No';
                $stringSupport  = base64_encode(json_encode([
                    'php_version'           => $php_version,
                    'wordpress_version'     => $wp_version,
                    'woocommerce_version'   => $wc_version,
                    'server_name'           => $server_name,
                    'curl_exists'           => $curl_exists
                ]));
                global $EMISSARY_lang;
                ?>
                <div class="tablecontent emissary-settings" id="emissary-access-conf">
                    <?php 
                    $this->form_fields = $this->emissary_Form($this->settings);
                    parent::admin_options();
                    ?>
                </div>
                <?php
            }

            static function emissary_Form($settings = []) {
                global $EMISSARY_lang;
                
                $setup_form['key_production'] = array(
                    'title'       => __( $EMISSARY_lang->key_production, EMISSARY_PLUGIN),
                    'type'        => 'text',
                    'default'     => '',
                    'desc_tip'    => false,
                );

                $setup_form['cp_origin'] = array(
                    'title'       => 'Código postal de origen',
                    'type'        => 'number',
                    'default'     => 0,
                    'desc_tip'    => false,
                );

                $setup_form['extra_mount'] = array(
                    'title'       => 'Agregar/Quitar monto al envío',
                    'type'        => 'number',
                    'default'     => 0,
                    'desc_tip'    => false,
                );
                $setup_form['deliver_in_store'] = array(
                    'title'       => 'Habilitar entrega en tienda',
                    'type'        => 'checkbox',
                    'default'     => 0,
                    'desc_tip'    => false,
                );
                return $setup_form;
            }

        }
    }
}

add_action( 'woocommerce_shipping_init', 'emissary_shipping_init' );