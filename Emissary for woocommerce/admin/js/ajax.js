jQuery(document).ready(function() {
    apikey = jQuery('input[name=apikey]').val();
    account = jQuery('input[name=account]').val();

    jQuery(document).on('click', '#down_label', function(){ 
        url = window.location.search;
        url = url.replace('?post=','');
        url2 = url.split('&');

        jQuery('#loader').css("z-index", "1");

        var data = {
            action: 'create_shipment',
            order_id: url2[0]
        };

        jQuery.post(EmissaryAjax.ajaxurl, data, function(response) {
            jQuery('#meta-box-ship').show();
            jQuery('#meta-box-ship h7').hide();
            
            var res = response.split('|||');

            jQuery('#meta-box-ship .inside').prepend(res[0]);
            jQuery('#loader').css("z-index", "-1");
        });   
    });

    jQuery(document).on('click', '#emissary-customs-declarations', function(){
        // e.preventDefault();
        url = window.location.search;
        url = url.replace('?post=','');
        url2 = url.split('&');
    
        jQuery('#loader').css("z-index", "1");
        jQuery('#modal-loader').css("display", "block");
        
        var type = jQuery("select[name=type]").val();
        var incoterm = jQuery("select[name=incoterm]").val();
        var prod_length = jQuery("input[name=prod_length]").val();
        var user = jQuery("input[name=user]").val();
    
        var validation = false;
        var items = [];
        for(var i=0; i<prod_length; i++){
            if ( !jQuery("input[name=\"product["+ i +"]['name']\"]").val() || 
                !jQuery("input[name=\"product["+ i +"]['quantity']\"]").val() ||
                !jQuery("input[name=\"product["+ i +"]['weight']\"]").val() ||
                !jQuery("input[name=\"product["+ i +"]['value']\"]").val() ||
                !jQuery("select[name=\"product["+ i +"]['origin']\"]").val() ||
                !jQuery("input[name=\"product["+ i +"]['description']\"]").val()
             ) {
                 validation = true;
                 break;
             }

            items.push({
                "name": jQuery("input[name=\"product["+ i +"]['name']\"]").val(),
                "quantity":  jQuery("input[name=\"product["+ i +"]['quantity']\"]").val(),
                "weight": jQuery("input[name=\"product["+ i +"]['weight']\"]").val(),
                "value": jQuery("input[name=\"product["+ i +"]['value']\"]").val(),
                "origin": jQuery("select[name=\"product["+ i +"]['origin']\"]").val(),
                "description": jQuery("input[name=\"product["+ i +"]['description']\"]").val()
            });
        }

        if (validation){
            alert("Required all the mandatory fields");
            jQuery('#loader').css("z-index", "-1");
            jQuery('#modal-loader').css("display", "none");
        } else {
            var customsDeclaration = {
                    "incoterm": incoterm,
                    "type": type,
                    "certifiedBy": user,
                    "items": items
                };
        
            var data = {
                action: 'create_shipment',
                order_id: url2[0],
                customs: customsDeclaration
            };

            
            self.parent.tb_remove();
            jQuery('#modal-loader').css("display", "none");
        
            jQuery.post(EmissaryAjax.ajaxurl, data, function(response) {
                jQuery('#meta-box-ship').show();
                jQuery('#meta-box-ship h7').hide();
                
                var res = response.split('|||');
        
                jQuery('#meta-box-ship .inside').prepend(res[0]);
                jQuery('#loader').css("z-index", "-1");
            });  
        }
    })
});