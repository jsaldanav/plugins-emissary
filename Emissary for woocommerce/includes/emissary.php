<?php
class Emissary {

    public static function plugin_settings_link($links) {
        $href = 'admin.php?page=wc-settings&tab=shipping&section=emissary';
        $settings_link = "<a href='{$href}'>". __( 'Configuration', EMISSARY_PLUGIN ) ."</a>";
        array_unshift($links, $settings_link);

        return $links;
    }

    static public function shipping_methods($methods) {
        $methods[] = 'Emissary_Shipping_Method';

        return $methods;
    }

    public function emissary_request($api_url , $method = 'GET', $json = []) {
        $settings = get_option('woocommerce_emissary_settings');
        $api_key = $settings['key_production'];
        $api_base_url = 'https://emissary.mx'. $api_url;

        if ($method == 'POST') {    
            $wp_request_headers = array(
                'Content-Type' => 'application/json'
            ); 

            $wp_request_headers['key'] = $api_key;     
            $timeout = 100000;
            $request_data = [
                'method'    => $method,
                'timeout' => $timeout,
                'headers'   => $wp_request_headers,
                'body'      => $json
            ];
            $response = wp_remote_request($api_base_url, $request_data);
            return json_decode(wp_remote_retrieve_body($response));
        } else if ($method == 'GET') {
            $response = wp_remote_get($api_base_url, $json);
            return wp_remote_retrieve_body($response);
        } else {
            $response = 'Error: hubo un error en la petición.';
        }
        return $response;
    }
}