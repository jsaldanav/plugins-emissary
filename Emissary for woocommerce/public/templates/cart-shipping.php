<?php
/**
 * Shipping Methods Display
 *
 * In 2.1 we show methods per package. This allows for multiple methods per order if so desired.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-shipping.php.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>
<tr class="shipping">
    <th><?php echo wp_kses_post( $package_name ); ?></th>
    <td data-title="<?php echo esc_attr( $package_name ); ?>">
        <?php if ( 1 <= count( $available_methods ) ) : ?>
            <?php
            $header_t = '';
            $setting = get_option('woocommerce_emissary_settings');
            $group = 1;
            ?>

            <ul id="shipping_method">
                <?php
                    $list = array();
                    foreach ( $available_methods as $method ) {
                        $list[] = $method;
                    }
                    $tp;
                    $count = count($list);
                    for ($i = 0; $i < $count; $i++) {
                        for ($j = $i; $j < $count; $j++) {
                            $label_advanced1 = $list[$i]->get_meta_data();
                            $label_advanced2 = $list[$j]->get_meta_data();
                            if (isset($label_advanced1['deliveryCost']) && isset($label_advanced2['deliveryCost']) && $label_advanced1['deliveryCost'] > $label_advanced2['deliveryCost']) {
                                $tp = $list[$i];
                                $list[$i] = $list[$j];
                                $list[$j] = $tp;
                            }
                        }
                    }

                    $arrier_list = array();
                    foreach ( $list as $i => $method ) {
                        $header = $method->get_meta_data();
                        if (isset($header['shippingMethod']) && !in_array($header['shippingMethod'] ,$arrier_list)) {
                            $arrier_list[] = $header['shippingMethod'];
                        }
                    }

                    foreach ( $arrier_list as $carrier ) {
                        foreach ( $list as $method ) {
                            $header = $method->get_meta_data();
                            if ($header['shippingMethod'] == $carrier) {
                                $temp = base64_decode($header['label_description_Advance']);
                                echo '<li style="text-align: left;width:280px;padding-top: 5px;padding-bottom: 5px;"><input type="radio" style="" name="shipping_method['.$index.']" data-index="'.$index.'" id="shipping_method_'.$index.'_'.sanitize_title( $method->id ).'" value="'.esc_attr( $method->id ).'" class="shipping_method" '.checked( $method->id, $chosen_method, false ).' />
                                <label for="shipping_method_'.$index.'_'.sanitize_title( $method->id ).'">'.$temp.'</label></li>';
                            }
                        }
                    }
                    do_action( 'woocommerce_after_shipping_rate', $method, $index );
                ?>
            </ul>
        <?php elseif ( 1 === count( $available_methods ) ) :  ?>
            <?php
            $method = current( $available_methods );
            printf( '%3$s <input type="hidden" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d" value="%2$s" class="shipping_method" />', $index, esc_attr( $method->id ), base64_encode(base64_decode(wc_cart_totals_shipping_method_label($method), true)) === wc_cart_totals_shipping_method_label($method) ? base64_decode(wc_cart_totals_shipping_method_label( $method )) : wc_cart_totals_shipping_method_label( $method ) );
            do_action( 'woocommerce_after_shipping_rate', $method, $index );
            ?>
        <?php endif; ?>

        <?php if ( $show_package_details ) : ?>
            <?php echo '<p class="woocommerce-shipping-contents"><small>' . esc_html( $package_details ) . '</small></p>'; ?>
        <?php endif; ?>

    </td>
</tr>
